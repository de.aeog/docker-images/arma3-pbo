# PboTools CLI

Latest Image:


```registry.gitlab.com/de.aeog/docker-images/arma3-pbo:1.0.6```

## Description

Avaiable tools in this image 

* /usr/bin/convertwrp
* /usr/bin/defxy
* /usr/bin/dekey
* /usr/bin/dep3d
* /usr/bin/depax
* /usr/bin/depew
* /usr/bin/derap
* /usr/bin/dertm
* /usr/bin/detec
* /usr/bin/dewrp
* /usr/bin/dewss
* /usr/bin/extractpbo
* /usr/bin/makepbo
* /usr/bin/moveobject
* /usr/bin/pbodeps
* /usr/bin/queryaddons
* /usr/bin/rapify


Thanks to mikero for your amazing toolset :) 
