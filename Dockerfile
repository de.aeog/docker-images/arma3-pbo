FROM ubuntu:18.04 AS builder

RUN apt-get update && apt-get install -y --no-install-recommends \
		ca-certificates \
        build-essential \
        cmake \
        wget \
 && rm -rf /var/apt/lists/*

RUN mkdir /src && cd /src \
 && wget https://mikero.bytex.digital/api/download?filename=depbo-tools-0.7.25-linux-64bit.tgz -O depbo-tools-0.7.25-linux-64bit.tgz \
 && tar xfzp depbo-tools-0.7.25-linux-64bit.tgz

FROM ubuntu:18.04

RUN apt-get update && apt-get install -y --no-install-recommends \
		liblzo2-2 \
		libvorbis0a \
		libvorbisfile3 \
		libvorbisenc2 \
		libogg0 \
		jq \
		zip \
		rename \
 && rm -rf /var/apt/lists/*

COPY --from=0 /src/depbo-tools-0.7.25/lib/ /usr/local/lib/
COPY --from=0 /src/depbo-tools-0.7.25/bin/ /usr/bin/
RUN ldconfig

CMD [ "./pbo" ]
